# Template for Fast API
[![codecov](https://codecov.io/gh/Maua-Dev/back_fastAPI_template/branch/main/graph/badge.svg?token=M16VBNGBR3)](https://codecov.io/gh/Maua-Dev/back_fastAPI_template)


This is a template to create Fast API based repositories 

## First Steps:

### Create a python virtual enviroment:
    py -m venv venv

### Activate virtual enviroment (*windows*)
    venv\Scripts\activate

### Install requirements
    pip install -r requirements.txt

# Usage:

### Start server
    uvicorn src.main:app --reload

### Run tests
    pytest
